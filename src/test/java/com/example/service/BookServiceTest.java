package com.example.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.exception.BookNotFoundException;
import com.example.model.Book;
import com.example.repository.BooksRepository;

@SpringBootTest
public class BookServiceTest {

	@InjectMocks
	BooksService bookService;

	@Mock
	BooksRepository bookRepository;

	@Test
	public void saveBookTest() {

		Book book = new Book();
		book.setBookname("TB");
		book.setAuthor("TA");
		book.setPrice(500);

		bookService.saveBook(book);
		
		verify(bookRepository, times(1)).save(book);
	}

	@Test
	public void getBookByIdTest() throws BookNotFoundException {
		Book book = new Book();
		book.setBookid(1);
		book.setBookname("TB");
		book.setAuthor("TA");
		book.setPrice(500);

		when(bookRepository.findById(1)).thenReturn(Optional.of(book));

		Book result = bookService.getBooksById(1);

		assertEquals(1, result.getBookid());
		assertEquals("TB", result.getBookname());

	}

	@Test
	public void getBookByIdTestNegative() {

		try {
			bookService.getBooksById(1);
			fail("Expected an BookNotFoundException to be thrown");
		} catch (BookNotFoundException e) {
			System.out.println(e.getMessage());
			assertEquals("No book with id 1", e.getMessage());
		}

	}

	@Test
	public void getAllBooksTest() {
		Book book = new Book();
		book.setBookid(1);
		book.setBookname("TB");
		book.setAuthor("TA");
		book.setPrice(500);

		List<Book> list = new ArrayList<>();
		list.add(book);

		when(bookRepository.findAll()).thenReturn(list);

		List<Book> result = (List<Book>) bookService.getAllBooks();
		System.out.println("result--" + result.get(0).getBookname());

		assertEquals(1, result.size());

	}
	
	@Test
	public void updateBookTest() throws BookNotFoundException {
		Book book = new Book();
		book.setBookid(1);
		book.setBookname("TB");
		book.setAuthor("TA");
		book.setPrice(500);

		when(bookRepository.findById(1)).thenReturn(Optional.of(book));

		bookService.updateBook(book, 1);;

		verify(bookRepository, times(1)).save(book);

	}
	
	@Test
	public void deleteBookTest() throws BookNotFoundException {
		Book book = new Book();
		book.setBookid(1);
		book.setBookname("TB");
		book.setAuthor("TA");
		book.setPrice(500);

		when(bookRepository.findById(1)).thenReturn(Optional.of(book));

		bookService.deleteBook(1);

		verify(bookRepository, times(1)).deleteById(1);

	}
}
