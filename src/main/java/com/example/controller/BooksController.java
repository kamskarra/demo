package com.example.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.example.exception.BookNotFoundException;
import com.example.model.Book;
import com.example.service.BooksService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * Controller to perform CRUD operations on Book
 *
 */
@RestController
@Api(description = "CRUD operations on Book")
public class BooksController {

	@Autowired
	BooksService booksService;
	
	/**
	 * 
	 * @return list of books
	 */
	@GetMapping("/books")
	@ApiOperation(value = "Get all books")
	private ResponseEntity<Iterable<Book>> getAllBooks() {

		return ResponseEntity.status(HttpStatus.OK).body(booksService.getAllBooks());
	}
	
	@GetMapping("/books/getauthorwithbookscount")
	@ApiOperation(value = "Get author with no. of books")
	private ResponseEntity<List<Map<String,Integer>>> getAuthorWithNoOfBooks() {

		return ResponseEntity.status(HttpStatus.OK).body(booksService.getAuthorWithNoOfBooks());
	}
	
	/**
	 * 
	 * @param price price of the book
	 * @return list of books
	 */
	@GetMapping("/books/getbyprice/{price}")
	@ApiOperation(value = "Get all books with price less or equal to mentioned price")
	private ResponseEntity<List<Book>> getBooksWithPriceLessOrEqual(@PathVariable("price") int price) {

		return ResponseEntity.status(HttpStatus.OK).body(booksService.getBooksWithPriceLessOrEqual(price));
	}
	
	/**
	 * 
	 * @param pageNo pageNo
	 * @param pageSize pageSize
	 * @param sortBy sortBy
	 * @return list of sorted books
	 */
	@GetMapping("/books/getsortedbooks")
	@ApiOperation(value = "Gets list of sorted books")
	public ResponseEntity<List<Book>> getSortedBooks(@RequestParam(defaultValue = "0") int pageNo,
			@RequestParam(defaultValue = "5") int pageSize, @RequestParam(defaultValue = "bookid") String sortBy) {

		List<Book> list = booksService.getSortedBooks(pageNo, pageSize, sortBy);

		return new ResponseEntity<List<Book>>(list, HttpStatus.OK);
	}
	
	/**
	 * 
	 * @param bookid bookid
	 * @return a book with mentioned bookid
	 * @throws BookNotFoundException
	 */
	@GetMapping("/books/{bookid}")
	@ApiOperation(value = "Get a book with id")
	private ResponseEntity<Book> getParticularBook(@PathVariable("bookid") int bookid) throws BookNotFoundException {

		Book book = booksService.getBooksById(bookid);

		return ResponseEntity.status(HttpStatus.OK).body(book);
	}
	
	/**
	 * 
	 * @param book - details of book
	 * @return bookid
	 */
	@PostMapping("/books")
	@ApiOperation(value = "Save new book")
	private ResponseEntity<Integer> saveBook(@RequestBody Book book) {

		booksService.saveBook(book);
		return ResponseEntity.status(HttpStatus.CREATED).body(book.getBookid());
	}
	
	/**
	 * 
	 * @param book - details of book
	 * @return updated status
	 */
	@PutMapping("/books")
	@ApiOperation(value = "Update book")
	private ResponseEntity<String> updateBook(@RequestBody Book book) {

		booksService.updateBook(book, book.getBookid());
		return ResponseEntity.status(HttpStatus.OK).body("Updated Successfully");
	}
	
	/**
	 * 
	 * @param bookid bookid
	 * @return delete status
	 * @throws BookNotFoundException
	 */
	@DeleteMapping("/books/{bookid}")
	@ApiOperation(value = "Delete book")
	private ResponseEntity<String> deleteBook(@PathVariable("bookid") int bookid) throws BookNotFoundException {

		booksService.deleteBook(bookid);
		return ResponseEntity.status(HttpStatus.OK).body("Deleted Successfully");
	}
}
