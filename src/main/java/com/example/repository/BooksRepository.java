package com.example.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.example.model.Book;

public interface BooksRepository extends JpaRepository<Book, Integer> {

	// Native query
	@Query(value = "SELECT * FROM BOOK WHERE PRICE <= ?1", nativeQuery = true)
	List<Book> getBooksWithPriceLessOrEqual(int price);
	
	// Native query
	@Query(value = "select distinct author, (select count(*) from book b where  a.author=b.author) as bookcount from book a", nativeQuery = true)
	List<Map<String,Integer>> getAuthorWithNoOfBooks();

}
