package com.example.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.exception.BookNotFoundException;
import com.example.model.Book;
import com.example.repository.BooksRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Book's service class
 * @author User1
 *
 */
@Service
public class BooksService {
	
	private static final Logger LOGGER=LoggerFactory.getLogger(BooksService.class);
	
	@Autowired
	BooksRepository booksRepository;

	public Iterable<Book> getAllBooks() {
		return booksRepository.findAll();
	}

	public Book getBooksById(int id) throws BookNotFoundException {
		
		//return booksRepository.findById(id).get();
		Optional<Book> book = booksRepository.findById(id);

		if (book.isPresent()) {
			return book.get();
		} else {
			LOGGER.info(" Info - No book with id " + id);
			LOGGER.error("Error - No book with id " + id);
			LOGGER.warn("Warn - No book with id " + id);
			throw new BookNotFoundException("No book with id " + id);
		}
	}

	public List<Book> getBooksWithPriceLessOrEqual(int price) {
		return booksRepository.getBooksWithPriceLessOrEqual(price);
	}

	public List<Book> getSortedBooks(int pageNo, int pageSize, String sortBy) {
		Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

		Page<Book> pagedResult = booksRepository.findAll(paging);

		if (pagedResult.hasContent()) {
			return pagedResult.getContent();
		} else {
			return new ArrayList<Book>();
		}
	}

	public void saveBook(Book books) {
		booksRepository.save(books);
	}

	public void deleteBook(int id) throws BookNotFoundException {
		
		Optional<Book> book = booksRepository.findById(id);

		if (book.isPresent()) {
			booksRepository.deleteById(id);
		} else {
			throw new BookNotFoundException("No book with id " + id);
		}
	}

	public void updateBook(Book book, int bookid) {
		booksRepository.save(book);
	}
	
	public List<Map<String,Integer>> getAuthorWithNoOfBooks() {
		return booksRepository.getAuthorWithNoOfBooks();
	}

}